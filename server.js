const express = require('express');
const cors = require('cors');
const fs = require('fs');
const path = require('path');

const app = express();
const port = 3452;
const API_KEY = process.env.PLAY_API_KEY;
const dataPath = path.join(__dirname, 'gms_certified_props.json');

const dataOps = {
  read: () => {
    try {
      return fs.readFileSync(dataPath, 'utf8');
    } catch {
      return null;
    }
  },
  write: (rawData) => {
    fs.writeFileSync(dataPath, rawData);
    return rawData;
  }
};

app.use(cors());
app.use(express.raw({
  type: 'application/json',
  limit: '50mb'
}));

app.get(['/', '/update'], (_, res) => {
  const data = dataOps.read();

  if (!data) {
    return res.status(404).json({ message: 'No data found' });
  }

  res.set('Content-Type', 'application/json');
  res.send(data);
});

app.post('/update', (req, res) => {
  const apiKey = req.headers['api-key'];

  if (!apiKey) {
    return res.status(400).json({ message: 'API key is missing' });
  }

  if (apiKey !== API_KEY) {
    return res.status(401).json({ message: 'Invalid API key' });
  }

  try {
    const rawData = req.body.toString();
    const data = dataOps.write(rawData);
    res.json({
      message: 'Data inserted',
      data: JSON.parse(data)
    });
  } catch (error) {
    res.status(500).json({ message: 'Failed to insert data' });
  }
});

app.listen(port, () => console.log(`Server running at http://localhost:${port}`));
